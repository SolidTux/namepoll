use askama::Template;
use log::{debug, error, info};
use ring::rand::{self, SecureRandom};
use serde::{Deserialize, Serialize};
use std::{
    collections::{BTreeMap, BTreeSet, HashMap, HashSet},
    fs::File,
    path::{Path, PathBuf},
    sync::{Arc, Mutex},
};
use structopt::StructOpt;
use warp::{http::StatusCode, reject::Rejection, Filter, Reply};

#[derive(Template)]
#[template(path = "poll.html")]
struct PollTemplate {
    person: Person,
    names: BTreeMap<String, usize>,
    max_votes: usize,
    can_vote: bool,
    can_edit: bool,
}

#[derive(Template)]
#[template(path = "error.html")]
struct ErrorTemplate {
    error: String,
}

#[derive(Template)]
#[template(path = "results.html")]
struct ResultsTemplate {
    persons: Vec<(String, usize)>,
    votes: Vec<(String, usize)>,
    can_show_results: bool,
}

#[derive(Template)]
#[template(path = "name.html")]
struct NameTemplate {
    name: String,
    info: NameInfo,
    can_edit: bool,
}

type Person = String;

#[derive(Debug, Serialize, Deserialize, Default, Clone)]
struct Database {
    names: HashSet<String>,
    votes: HashMap<Person, HashMap<String, usize>>,
    info: HashMap<String, NameInfo>,
}

#[derive(Debug, Serialize, Deserialize, Default, Clone)]
struct NameInfo {
    urls: BTreeSet<String>,
}

impl Database {
    fn save(&self, path: &Path) -> Result<(), failure::Error> {
        let file = File::create(path)?;
        serde_json::to_writer_pretty(file, self)?;
        Ok(())
    }

    fn load(path: &Path) -> Result<Self, failure::Error> {
        Ok(match File::open(path) {
            Ok(f) => serde_json::from_reader(f)?,
            Err(_) => Self::default(),
        })
    }
}

#[derive(Debug, Clone, Default, Serialize)]
struct State {
    database: Database,
    config: Config,
    tokens: HashMap<String, Person>,
}

impl State {
    fn get_user(&self, token: &str) -> Option<String> {
        self.tokens.get(token).cloned()
    }

    fn has_permission(&self, user: &str, perm: Permission) -> bool {
        self.config
            .permissions
            .get(user)
            .map(|p| p.contains(&perm))
            .unwrap_or(false)
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
struct Config {
    db_path: PathBuf,
    users: HashMap<String, Person>,
    permissions: HashMap<String, Vec<Permission>>,
    max_votes: usize,
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Eq)]
#[serde(rename_all = "snake_case")]
enum Permission {
    EditNames,
    Vote,
    ShowResults,
    ShowVotes,
    Debug,
}

impl Config {
    fn load(path: &Path) -> Result<Self, failure::Error> {
        Ok(match File::open(path) {
            Ok(f) => serde_json::from_reader(f)?,
            Err(_) => Self::default(),
        })
    }
}

impl Default for Config {
    fn default() -> Self {
        Config {
            db_path: "db.json".into(),
            users: HashMap::new(),
            permissions: HashMap::new(),
            max_votes: 0,
        }
    }
}

#[derive(Debug, Clone, Copy)]
enum Error {
    Internal,
    Forbidden,
    InvalidVote,
    InvalidQuery,
}

impl Error {
    fn status(&self) -> StatusCode {
        match self {
            Error::Internal => StatusCode::INTERNAL_SERVER_ERROR,
            Error::Forbidden => StatusCode::FORBIDDEN,
            Error::InvalidVote => StatusCode::OK,
            Error::InvalidQuery => StatusCode::OK,
        }
    }
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            Error::Internal => "Internal Server Error",
            Error::Forbidden => "Forbidden",
            Error::InvalidVote => "Invalid Vote",
            Error::InvalidQuery => "Invalid Query",
        })
    }
}

impl std::error::Error for Error {}

fn temporary_redirect(path: &str) -> impl Reply {
    warp::reply::with_header(
        warp::reply::with_status("", StatusCode::FOUND),
        "Location",
        path,
    )
}

fn add_name(
    state: Arc<Mutex<State>>,
    parameters: HashMap<String, String>,
) -> Result<impl Reply, Rejection> {
    let mut state = state
        .lock()
        .map_err(|_| warp::reject::custom(Error::Internal))?;
    if let Some(name) = parameters.get("name") {
        state.database.names.insert(name.clone());
        state
            .database
            .save(&state.config.db_path)
            .map_err(|_| warp::reject::custom(Error::Internal))?;
    }
    Ok(temporary_redirect("/"))
}

fn remove_name(
    state: Arc<Mutex<State>>,
    parameters: HashMap<String, String>,
) -> Result<impl Reply, Rejection> {
    let mut state = state
        .lock()
        .map_err(|_| warp::reject::custom(Error::Internal))?;
    if let Some(name) = parameters.get("name") {
        state.database.names.remove(name);
        state
            .database
            .save(&state.config.db_path)
            .map_err(|_| warp::reject::custom(Error::Internal))?;
    }
    Ok(temporary_redirect("/"))
}

fn debug(state: Arc<Mutex<State>>) -> Result<impl Reply, Rejection> {
    let state = &state
        .lock()
        .map_err(|_| warp::reject::custom(Error::Internal))?;
    Ok(warp::reply::json(&(*state).clone()))
}

fn token(
    state: Arc<Mutex<State>>,
    parameters: HashMap<String, String>,
) -> Result<impl Reply, Rejection> {
    let mut state = state
        .lock()
        .map_err(|_| warp::reject::custom(Error::Internal))?;
    let token = parameters
        .get("token")
        .ok_or(warp::reject::custom(Error::Internal))?;
    let mut user = None;
    for (hash, person) in &state.config.users {
        if let Ok(true) = bcrypt::verify(token, hash) {
            info!("Authenticated as user {}", person);
            user = Some(person.to_string());
            break;
        }
    }
    if let Some(u) = user {
        let rng = rand::SystemRandom::new();
        let mut token = [0u8; 16];
        rng.fill(&mut token)
            .map_err(|_| warp::reject::custom(Error::Internal))?;
        let t = base64::encode(&token);
        state.tokens.insert(t.clone(), u);

        Ok(warp::reply::with_header(
            temporary_redirect("/"),
            "Set-Cookie",
            format!("token={}", t),
        ))
    } else {
        Ok(warp::reply::with_header(
            temporary_redirect("/"),
            "Set-Cookie",
            "token=",
        ))
    }
}

fn index(state: Arc<Mutex<State>>, token: Option<String>) -> Result<impl Reply, Rejection> {
    let state = state
        .lock()
        .map_err(|_| warp::reject::custom(Error::Internal))?;
    match token.and_then(|t| state.tokens.get(&t).map(|x| (x, t))) {
        Some((person, t)) => {
            debug!("Token: {}", t);
            let mut names = BTreeMap::new();
            for n in &state.database.names {
                names.insert(
                    n.to_string(),
                    state
                        .database
                        .votes
                        .get(person)
                        .and_then(|v| v.get(n))
                        .cloned()
                        .unwrap_or(0),
                );
            }
            let poll = PollTemplate {
                person: person.clone(),
                names,
                max_votes: state.config.max_votes,
                can_vote: state.has_permission(&person, Permission::Vote),
                can_edit: state.has_permission(&person, Permission::EditNames),
            };
            let body = poll
                .render()
                .map_err(|_| warp::reject::custom(Error::Internal))?;
            Ok(warp::reply::with_header(
                warp::reply::html(body),
                "Set-Cookie",
                format!("token={}", t),
            ))
        }
        None => Ok(warp::reply::with_header(
            warp::reply::html(include_str!("../static/token.html").to_string()),
            "Set-Cookie",
            "token=",
        )),
    }
}

fn info(
    (state, user): (Arc<Mutex<State>>, String),
    parameters: HashMap<String, String>,
) -> Result<impl Reply, Rejection> {
    let state = state
        .lock()
        .map_err(|_| warp::reject::custom(Error::Internal))?;
    if let Some(name) = parameters.get("name") {
        debug!("User: {} Name: {}", user, name);
        if state.database.names.contains(name) {
            let info = state.database.info.get(name).cloned().unwrap_or_default();
            let body = NameTemplate {
                name: name.to_string(),
                info,
                can_edit: state.has_permission(&user, Permission::EditNames),
            }
            .render()
            .map_err(|_| warp::reject::custom(Error::Internal))?;
            Ok(warp::reply::html(body))
        } else {
            Err(warp::reject::custom(Error::InvalidQuery))
        }
    } else {
        Err(warp::reject::custom(Error::InvalidQuery))
    }
}

fn add_url(
    state: Arc<Mutex<State>>,
    parameters: HashMap<String, String>,
) -> Result<impl Reply, Rejection> {
    let mut state = state
        .lock()
        .map_err(|_| warp::reject::custom(Error::Internal))?;
    if let Some(name) = parameters.get("name") {
        if let Some(url) = parameters.get("url") {
            let url = if url.starts_with("http") {
                url.to_string()
            } else {
                "http://".to_string() + url
            };
            let e = state.database.info.entry(name.clone()).or_default();
            e.urls.insert(url.to_string());
            state
                .database
                .save(&state.config.db_path)
                .map_err(|_| warp::reject::custom(Error::Internal))?;
        }
        return Ok(temporary_redirect(&format!("/info?name={}", name)));
    }
    Err(warp::reject::custom(Error::InvalidQuery))
}

fn remove_url(
    state: Arc<Mutex<State>>,
    parameters: HashMap<String, String>,
) -> Result<impl Reply, Rejection> {
    let mut state = state
        .lock()
        .map_err(|_| warp::reject::custom(Error::Internal))?;
    if let Some(name) = parameters.get("name") {
        if let Some(url) = parameters.get("url") {
            let e = state.database.info.entry(name.clone()).or_default();
            e.urls.remove(url);
            state
                .database
                .save(&state.config.db_path)
                .map_err(|_| warp::reject::custom(Error::Internal))?;
        }
        return Ok(temporary_redirect(&format!("/info?name={}", name)));
    }
    Err(warp::reject::custom(Error::InvalidQuery))
}

fn submit(
    (state, user): (Arc<Mutex<State>>, String),
    votes: HashMap<String, usize>,
) -> Result<impl Reply, Rejection> {
    let mut state = state
        .lock()
        .map_err(|_| warp::reject::custom(Error::Internal))?;
    let config = state.config.clone();
    let database = &mut state.database;
    let mut v = HashMap::new();
    let mut sum = 0;
    for (name, value) in votes {
        if database.names.contains(&name) {
            v.insert(name, value);
            sum += value;
        }
    }
    if sum > config.max_votes {
        return Err(warp::reject::custom(Error::InvalidVote));
    } else {
        database.votes.remove(&user);
        database.votes.insert(user.clone(), v);
        database
            .save(&config.db_path)
            .map_err(|_| warp::reject::custom(Error::Internal))?;
    }
    Ok(temporary_redirect("/"))
}

fn results((state, user): (Arc<Mutex<State>>, String)) -> Result<impl Reply, Rejection> {
    let state = state
        .lock()
        .map_err(|_| warp::reject::custom(Error::Internal))?;
    let mut votes = HashMap::new();
    let mut persons = Vec::new();
    for (p, v) in &state.database.votes {
        let mut sum = 0;
        for (name, value) in v {
            let e = votes.entry(name.clone()).or_insert(0);
            sum += value;
            *e += value;
        }
        persons.push((p.clone(), sum));
    }
    persons.sort_by_key(|a| a.0.clone());
    let mut votes = votes.drain().collect::<Vec<_>>();
    votes.sort_by_key(|&(_, b)| -(b as isize));
    let body = ResultsTemplate {
        persons,
        votes,
        can_show_results: state.has_permission(&user, Permission::ShowResults),
    }
    .render()
    .map_err(|_| warp::reject::custom(Error::Internal))?;
    Ok(warp::reply::html(body))
}

fn check_token(
    state: Arc<Mutex<State>>,
    token: String,
) -> Result<(Arc<Mutex<State>>, String), warp::Rejection> {
    let s = state
        .lock()
        .map_err(|_| warp::reject::custom(Error::Internal))?;
    if let Some(u) = s.get_user(&token) {
        Ok((state.clone(), u.to_string()))
    } else {
        Err(warp::reject::custom(Error::Forbidden))
    }
}

fn check_permission(
    state: Arc<Mutex<State>>,
    user: String,
    perm: Permission,
) -> Result<Arc<Mutex<State>>, warp::Rejection> {
    let s = state
        .lock()
        .map_err(|_| warp::reject::custom(Error::Internal))?;
    if s.has_permission(&user, perm) {
        Ok(state.clone())
    } else {
        Err(warp::reject::custom(Error::Forbidden))
    }
}

fn logout(state: Arc<Mutex<State>>, token: String) -> Result<impl Reply, Rejection> {
    let mut state = state
        .lock()
        .map_err(|_| warp::reject::custom(Error::Internal))?;
    state.tokens.remove(&token);
    Ok(warp::reply::with_header(
        temporary_redirect("/"),
        "Set-Cookie",
        "token=",
    ))
}

fn recover(err: Rejection) -> Result<impl Reply, Rejection> {
    error!("{:?}", err);
    let (error, code) = if let Some(&error) = err.find_cause::<Error>() {
        (format!("{}", error), error.status())
    } else if let Some(error) = &err.find_cause::<warp::reject::InvalidQuery>() {
        (format!("{}", error), StatusCode::OK)
    } else {
        (
            "Internal Server Error".to_string(),
            StatusCode::INTERNAL_SERVER_ERROR,
        )
    };
    let template = ErrorTemplate { error };
    match template.render() {
        Ok(body) => Ok(warp::reply::with_status(warp::reply::html(body), code)),
        Err(_) => Err(err),
    }
}

#[derive(Debug, StructOpt)]
struct Opt {
    #[structopt(short = "q", long = "quiet")]
    quiet: bool,
    #[structopt(short = "v", long = "verbose", parse(from_occurrences))]
    verbose: usize,
    #[structopt(default_value = "8080")]
    port: u16,
}

fn main() -> Result<(), failure::Error> {
    let opt = Opt::from_args();
    stderrlog::new()
        .quiet(opt.quiet)
        .verbosity(opt.verbose)
        .init()?;

    let config = Config::load("config.json".as_ref())?;
    let db = Database::load(&config.db_path)?;
    db.save(&config.db_path)?;

    let s = Arc::new(Mutex::new(State {
        database: db,
        config: config,
        tokens: HashMap::new(),
    }));
    let state = warp::any().map(move || s.clone());

    let index = warp::path::end()
        .and(state.clone())
        .and(warp::cookie::optional("token"))
        .and_then(index);
    let add = warp::path("add")
        .and(state.clone())
        .and(warp::cookie::cookie("token"))
        .and_then(check_token)
        .and_then(|(s, t)| check_permission(s, t, Permission::EditNames))
        .and(warp::query::query())
        .and_then(add_name);
    let remove = warp::path("remove")
        .and(state.clone())
        .and(warp::cookie::cookie("token"))
        .and_then(check_token)
        .and_then(|(s, t)| check_permission(s, t, Permission::EditNames))
        .and(warp::query::query())
        .and_then(remove_name);
    let token = warp::path("token")
        .and(state.clone())
        .and(warp::body::form())
        .and_then(token);
    let debug = warp::path("debug")
        .and(state.clone())
        .and(warp::cookie::cookie("token"))
        .and_then(check_token)
        .and_then(|(s, t)| check_permission(s, t, Permission::Debug))
        .and_then(debug);
    let logout = warp::path("logout")
        .and(state.clone())
        .and(warp::cookie::cookie("token"))
        .and_then(logout);
    let submit = warp::path("submit")
        .and(state.clone())
        .and(warp::cookie::cookie("token"))
        .and_then(check_token)
        .and_then(|(s, t)| check_permission(s, t, Permission::Vote))
        .and(warp::cookie::cookie("token"))
        .and_then(check_token)
        .and(warp::query::query())
        .and_then(submit);
    let results = warp::path("results")
        .and(state.clone())
        .and(warp::cookie::cookie("token"))
        .and_then(check_token)
        .and_then(results);
    let info = warp::path("info")
        .and(state.clone())
        .and(warp::cookie::cookie("token"))
        .and_then(check_token)
        .and(warp::query::query())
        .and_then(info);
    let add_url = warp::path("add_url")
        .and(state.clone())
        .and(warp::cookie::cookie("token"))
        .and_then(check_token)
        .and_then(|(s, t)| check_permission(s, t, Permission::EditNames))
        .and(warp::query::query())
        .and_then(add_url);
    let remove_url = warp::path("remove_url")
        .and(state.clone())
        .and(warp::cookie::cookie("token"))
        .and_then(check_token)
        .and_then(|(s, t)| check_permission(s, t, Permission::EditNames))
        .and(warp::query::query())
        .and_then(remove_url);
    let css = warp::path("style.css").map(|| {
        warp::reply::with_header(
            include_str!("../static/style.css"),
            "content-type",
            "text/css",
        )
    });

    let routes = warp::get2()
        .and(
            index
                .or(add)
                .or(remove)
                .or(debug)
                .or(submit)
                .or(logout)
                .or(results)
                .or(info)
                .or(add_url)
                .or(remove_url)
                .or(css),
        )
        .or(warp::post2().and(token))
        .recover(recover)
        .with(warp::log("namepoll"));

    warp::serve(routes).run(([0, 0, 0, 0], opt.port));

    Ok(())
}
